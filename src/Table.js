import React from 'react';
import PropTypes from 'prop-types';
import ReactStars from 'react-stars';

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sorted: '',
            sortedBy: '',
            sortForType: ''
        };
    };

    clearNumber = (value) => {
        if(typeof(value) === 'number')
            return value
        else
            return parseInt(value.replace(/\$/g, ''), 10);
    }

    sortNumByAsc = (a, b) => {
        const sortingField = this.state.sortedBy;
        let parsedA = this.clearNumber(a[sortingField]);
        let parsedB = this.clearNumber(b[sortingField]);

        if (parsedA < parsedB) return -1;
        if (parsedA > parsedB) return 1;
        return 0;
    };

    sortNumByDesc = (a, b) => {
        const sortingField = this.state.sortedBy;
        let parsedA = this.clearNumber(a[sortingField]);
        let parsedB = this.clearNumber(b[sortingField]);

        if (parsedA > parsedB) return -1;
        if (parsedA < parsedB) return 1;
        return 0;
    };
    
    sortStrByAsc = (a, b) => {
        const sortingField = this.state.sortedBy;
        if (a[sortingField].toLowerCase() < b[sortingField].toLowerCase()) return -1;
        if (a[sortingField].toLowerCase() > b[sortingField].toLowerCase()) return 1;
        return 0;
    };

    sortStrByDesc = (a, b) => {
        const sortingField = this.state.sortedBy;
        if (a[sortingField].toLowerCase() > b[sortingField].toLowerCase()) return -1;
        if (a[sortingField].toLowerCase() < b[sortingField].toLowerCase()) return 1;
        return 0;
    };

    getFilterData = (data, activeFilters) => {
        const filteredData = [];

        if(activeFilters.length <= 0)
            return data;

        data.forEach( (item) => {
            if(activeFilters.includes(item.type)) {
                filteredData.push(item);
            }
        });

        return filteredData;
    }

    getSortedData = (data) => {
        if(this.state.sorted === '') {
            return data;
        }
        if(this.state.sortForType === 'number') {
            const compare = this.state.sorted === 'asc' ? this.sortNumByAsc : this.sortNumByDesc;
            return data.sort(compare);
        } else {
            const compare = this.state.sorted === 'asc' ? this.sortStrByAsc : this.sortStrByDesc;
            return data.sort(compare);
        }
    }

    handleSortClick = (e) => {
        const sortBy = e.target.title;
        let nextSorting;
        let sortForType;

        if(this.state.sorted === '' || this.state.sortedBy !== sortBy) {
            nextSorting = 'asc';
        } else {
            nextSorting = this.state.sorted === 'asc' ? 'desc' : 'asc';
        }

        if( sortBy === 'name' ) {
            sortForType = 'string';
        } else {
            sortForType = 'number';
        }



        this.setState({ 
            sorted: nextSorting, 
            sortedBy: sortBy,
            sortForType: sortForType
        });
    };

    render() {
        const filteredData = this.getFilterData(this.props.tableData, this.props.filterBy);
        const sortedData = this.getSortedData(filteredData);

        return (
            <div className="Table">
                <div className="Table__heading">
                    <span onClick={this.handleSortClick} 
                        title="name" 
                        className="Table__heading--name"
                        data-sorted={this.state.sorted}
                        data-sortedby={this.state.sortedBy}>Product Name</span>
                    <span onClick={this.handleSortClick}
                        title="rating" 
                        className="Table__heading--rating"
                        data-sorted={this.state.sorted}
                        data-sortedby={this.state.sortedBy}>Rating</span>
                    <span onClick={this.handleSortClick} 
                        title="price" 
                        className="Table__heading--price"
                        data-sorted={this.state.sorted}
                        data-sortedby={this.state.sortedBy}>Price</span>
                </div>
                <div className="Table__wrapper">
                    <table className="Table__table">
                        <tbody>
                        { sortedData.map((item, i) => {
                            return (
                               <tr key={i} className="Table__row">
                                    <td className="Table__column--name">{item.name}</td>
                                    <td className="Table__column--rating">
                                        <ReactStars
                                           count={5}
                                           color2={'#ffd700'}
                                           edit={ false }
                                           size={16}
                                           value={ item.rating }
                                        />
                                    </td>
                                    <td className="Table__column--price">{item.price}</td>
                               </tr>
                               )
                        })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Table;

Table.propTypes = {
    tableData: PropTypes.array,
    activeFilters: PropTypes.array
}