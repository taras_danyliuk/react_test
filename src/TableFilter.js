import React from 'react';
import PropTypes from 'prop-types';
import FilterItem from './FilterItem';

class TableFilter extends React.Component {

    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <div className="TableFilter__wrapper">
                {
                    this.props.filterItems.map( (item, i) => 
                        <FilterItem item={item} 
                                    key={i}
                                    onItemChange={this.props.handleFilterChange}
                        />
                    )
                }
            </div>
        );
    }
}

export default TableFilter;

TableFilter.propTypes = {
    filterItems: PropTypes.array,
    handleFilterChange: PropTypes.func,
}