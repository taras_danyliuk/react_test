import React from 'react';
import PropTypes from 'prop-types';
import TableFilter from './TableFilter';
import Table from './Table';

import './TablePage.css';

class TablePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeFilters: []
        }
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }

    getUniqueTypes = (data) => {
        let uniqueTypes = [];

        data.forEach( object => { 
            if (!uniqueTypes.includes(object.type)) 
                uniqueTypes.push(object.type);
        });

        return uniqueTypes;
    }

    handleFilterChange(e) {

        const value = e.target.value;
        // console.log('handleFilterChange ', e.target.value);
        let prevFilters = [].concat(this.state.activeFilters);
        let updatedFilters = [];

        if(!prevFilters.includes(value)) {
            updatedFilters = prevFilters.concat(value);
        } else {
            let index = prevFilters.indexOf(value);
            updatedFilters = prevFilters.slice(0, index).concat(prevFilters.slice(index + 1));
        }

        this.setState({
            activeFilters: updatedFilters
        });
    }

    render() {
        return (
            <div className="TablePage">
                <TableFilter 
                    filterItems={ this.getUniqueTypes(this.props.data) } 
                    handleFilterChange={this.handleFilterChange}
                />  
                <Table tableData={ this.props.data } filterBy={this.state.activeFilters}/>
            </div>
        ); 
    }
}

export default TablePage;

TablePage.propTypes = {
    data: PropTypes.array
}