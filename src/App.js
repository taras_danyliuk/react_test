import React, { Component } from 'react';
import TablePage from './TablePage.js';
import './App.css';

const table_data = [
  {
    "id": 0,
    "type": "phone",
    "name": "Xiaomi redmi 3s",
    "price": "$130",
    "rating": 3.2
  }, {
    "id": 1,
    "type": "phone",
    "name": "iPhone 6",
    "price": "$453",
    "rating": 4.1
  }, {
    "id": 2,
    "type": "laptop",
    "name": "Apple Macbook Pro",
    "price": "$1578",
    "rating": 3.9
  }, {
    "id": 3,
    "type": "mp3 player",
    "name": "Sony - Walkman NW-E395",
    "price": "$76",
    "rating": 4.6
  }, {
    "id": 4,
    "type": "mp3 player",
    "name": "SanDisk - Clip Jam",
    "price": "$29",
    "rating": 2.4
  }, {
    "id": 5,
    "type": "phone",
    "name": "HTC One V",
    "price": "$112",
    "rating": 1.8
  }, {
    "id": 6,
    "type": "mp3 player",
    "name": "Apple iPod Nano",
    "price": "$67",
    "rating": 4.3
  }, {
    "id": 7,
    "type": "mp3 player",
    "name": "Sony Walkman Street",
    "price": "$29",
    "rating": 1.2
  }, {
    "id": 8,
    "type": "phone",
    "name": "Meizu 6",
    "price": "$357",
    "rating": 3.3
  }, {
    "id": 9,
    "type": "phone",
    "name": "Google Pixel 2",
    "price": "$659",
    "rating": 4.8
  }, {
    "id": 10,
    "type": "phone",
    "name": "Apple iPhone 6s",
    "price": "$496",
    "rating": 4.7
  }, {
    "id": 11,
    "type": "laptop",
    "name": "Apple Macbook Air",
    "price": "$861",
    "rating": 4.1
  }, {
    "id": 12,
    "type": "laptop",
    "name": "Microsoft Surface Pro",
    "price": "$799",
    "rating": 3.5
  }, {
    "id": 13,
    "type": "phone",
    "name": "Xiaomi Mi 4s",
    "price": "$330",
    "rating": 2.6
  }, {
    "id": 14,
    "type": "laptop",
    "name": "Asus 550cc",
    "price": "$628",
    "rating": 1.2
  }, {
    "id": 15,
    "type": "phone",
    "name": "Apple iPhone 6s",
    "price": "$496",
    "rating": 4.7
  }, {
    "id": 16,
    "type": "laptop",
    "name": "Apple Macbook Air",
    "price": "$861",
    "rating": 4.1
  }, {
    "id": 17,
    "type": "laptop",
    "name": "Microsoft Surface Pro",
    "price": "$799",
    "rating": 3.5
  }, {
    "id": 18,
    "type": "phone",
    "name": "Xiaomi Mi 4s",
    "price": "$330",
    "rating": 2.6
  }, {
    "id": 19,
    "type": "laptop",
    "name": "Asus 550cc",
    "price": "$628",
    "rating": 1.2
  }
];

class App extends Component {

  _addNewType() {
    table_data.push({ id: 20, type: "tablet", name: "Lenovo xxx", price: "$123", rating: 2.2 });
    this.forceUpdate();
  }

  render() {
    return (
      <div className="App">
        <button onClick={this._addNewType.bind(this)}>Add new type</button>

        <TablePage data={ table_data } />
      </div>
    );
  }
}

export default App;
