import React from 'react';
import PropTypes from 'prop-types';

class TableItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
        }
        this.onFilterChange = this.onFilterChange.bind(this);
    }

    onFilterChange(e) {
        
        this.props.onItemChange(e);
        
        this.setState({
            checked: !this.state.checked
        });
        // console.log('onFilterChange ', e.target.value);
    }

    render() {
        return (
            <div className="TableFilter__row">
                <label className="TableFilter__label">
                    <input type="checkbox" 
                            value={this.props.item}
                            checked={this.state.checked}
                            onChange={this.onFilterChange}
                            className="TableFilter__input"
                    /> 
                    <span className="TableFilter__span">{`${this.props.item}s`}</span>
                </label>
            </div>
        )
    }
}

export default TableItem;

TableItem.propTypes = {
    item: PropTypes.string,
    onItemChange: PropTypes.func,
}